﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
	enum ClassGroups {EA1,EA2,EB1};
    class Student
    {
        public string Name { get; set; }
		private byte age;
		public static void TestStudent()
		{
			OOP.Student student1 = new Student();
			student1.Classgroup = ClassGroups.EA1;
			student1.Age = 21;
			student1.Name = "Joske Vermeulen";
			student1.MarkCommunication = 15;
			student1.MarkProgrammingPrinciples = 15;
			student1.MarkWebtech = 13;
			Console.WriteLine(student1.ShowOverView());

			OOP.Student student2 = new Student();
			student2.Classgroup = ClassGroups.EB1;
			student2.Age = 122;
			student2.Name = "Jefke Vermeulen";
			student2.MarkCommunication = 18;
			student2.MarkProgrammingPrinciples = 15;
			student2.MarkWebtech = 12;
			Console.WriteLine(student2.ShowOverView());
			Console.ReadKey();
		}
		public byte Age
		{
			get { return age; }
			set
			{
				if (value <= 120)
				{
					age = value;
				}
				else
				{
					age = 0;
				}
			}
		}

		public ClassGroups Classgroup { get; set; }

		private byte markCommunication;

		public byte MarkCommunication
		{
			get { return markCommunication; }
			set {
				if (value <= 20)
				{
					markCommunication = value;
				}
			}
		}
		private byte markProgrammingPrinciples;

		public byte MarkProgrammingPrinciples
		{
			get { return markProgrammingPrinciples; }
			set {
				if (value <= 20)
				{
					markProgrammingPrinciples = value;
				}
			}
		}

		private byte markWebTech;

		public byte MarkWebtech
		{
			get { return markWebTech; }
			set {
				if (value <= 20)
				{
					markWebTech = value;
				}
			}
		}

		public float OverAllMark { get
			{
				return (markWebTech + markProgrammingPrinciples + markCommunication) / 3;
			} 
		}

		public string ShowOverView()
		{
			string view = $@"
			{Name}, {(Age==0 ? "Ongeldige leeftijd" : Age.ToString() +"jaar")} 
			Klas: {Classgroup}

			Cijferrapport:
			--------------
			Communicatie:	{MarkCommunication}
			Programming Principles:	{MarkProgrammingPrinciples}
			Web Technology:	{MarkWebtech}
			Gemiddelde:	{OverAllMark}";
			return view;
		}
	}
}
