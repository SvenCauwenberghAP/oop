﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OOP
{
    class LerenWerkenMetDateTime
    {
        public static void SubMenuDateTime()
        {
            bool done = false;
            Console.Clear();
            while (!done)
            {
                Console.WriteLine("H8 DateTime: leren werken met objecten");
                Console.WriteLine("1: H8 Dag van de week");
                Console.WriteLine("2: H8 Schrikkeljaar teller");
                Console.WriteLine("3: H8 Ticks sinds 2000");
                Console.WriteLine("4: H8 Simple timing");
                Console.WriteLine("5: H8 Blaffende honden");
                Console.WriteLine("6: H8 Getallencombinatie");
                Console.WriteLine("Q. Terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        OOP.DayOfWeekProgram.Main();
                        break;
                    case '2':
                        OOP.LeapYearProgram.Main();
                        break;
                    case '3':
                        OOP.Ticks2000Program.Main();
                        break;
                    case '4':
                        OOP.ArrayTimerProgram.Main();
                        break;
                    case '5':
                        OOP.BarkingDog.BarkingDogs();
                        break;
                    case '6':
                        OOP.GetallenCombinatie.Getallen();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }
    }
}
