﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Auto
    {
        public double KilometerTeller { get; set; }
        public double Snelheid { get; set; }

        public Auto()
        {
            KilometerTeller = 0;
            Snelheid = 0;
        }
        public double Remmen()
        {
            return 0;
        }

        public void GasGeven()
        {
            Snelheid += 10;
            KilometerTeller += (Snelheid / 3600 * 100);
        }

        public static void AutoLatenRijden()
        {
            Auto car1 = new Auto();
            for (int i = 1; i <= 5; i++)
            {
                car1.GasGeven();
            }
            for (int i = 1; i <= 3; i++)
            {
                car1.Remmen();
            }
            Auto car2 = new Auto();
            for (int i = 1; i <= 10; i++)
            {
                car2.GasGeven();
            }
            car2.Remmen();
            Console.WriteLine($"Auto 1: {car1.Snelheid:f2}km/u, afgelegde weg {car1.KilometerTeller:f2}km");
            Console.WriteLine($"Auto 2: {car2.Snelheid:f2}km/u, afgelegde weg {car2.KilometerTeller:f2}km");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
