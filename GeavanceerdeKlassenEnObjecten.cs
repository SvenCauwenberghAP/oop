﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace OOP
{
    class GeavanceerdeKlassenEnObjecten
    {
        
        public static void SpelenMetSTrings()
        {
            WebClient wc = new WebClient();
            string csv = wc.DownloadString("http://samplecsvs.s3.amazonaws.com/SalesJan2009.csv");
            string[] split = csv.Split(Environment.NewLine.ToCharArray());

            Console.WriteLine();

            for (int i = 1; i < split.Length; i++)
            {
                string[] lijnsplit = split[i].Split(',');
                Console.WriteLine($"{lijnsplit[0]} , {lijnsplit[1]} , {lijnsplit[2]}!");
            }
            Console.ReadLine();

        }
    }
}
