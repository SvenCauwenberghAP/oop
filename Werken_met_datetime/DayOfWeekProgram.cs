﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;


namespace OOP
{
    class DayOfWeekProgram
    {
        public static void Main()
        {
            Console.Write("Welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.Write("Welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.Write("Welk jaar?");
            int year = int.Parse(Console.ReadLine());
            DateTime dateTime = new DateTime(year, month, day);
            CultureInfo belgianCI = new CultureInfo("nl-BE");
            Console.WriteLine($"{dateTime.ToString("dd MMMM yyyy", belgianCI)} is een {dateTime.ToString("dddd", belgianCI)}");
        }
    }
}
