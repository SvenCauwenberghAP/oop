﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV2
    {
        public byte Percentage
        {
            get;
            set;
        }

        public enum Honors
        {
            NietGeslaagd,
            Voldoende,
            Onderscheiding,
            GroteOnderscheiding,
            GrootsteOnderscheiding
        }
        public Honors ComputeHonors()
        {
            if (Percentage < 50)
            {
                return Honors.NietGeslaagd;
            }
            else if (Percentage <= 68)
            {
                return Honors.Voldoende;
            }
            else if (Percentage <= 75)
            {
                return Honors.Onderscheiding;
            }
            else if (Percentage <= 85)
            {
                return Honors.GroteOnderscheiding;
            }
            else
            {
                return Honors.GrootsteOnderscheiding;
            }
        }

        public static void StartRapportModule2()
        {
            ResultV2 result1 = new ResultV2();
            result1.Percentage = 40;
            Console.WriteLine(result1.ComputeHonors());
            ResultV2 result2 = new ResultV2();
            result2.Percentage = 60;
            Console.WriteLine(result2.ComputeHonors());
            ResultV2 result3 = new ResultV2();
            result3.Percentage = 70;
            Console.WriteLine(result3.ComputeHonors());
            ResultV2 result4 = new ResultV2();
            result4.Percentage = 80;
            Console.WriteLine(result4.ComputeHonors());
            ResultV2 result5 = new ResultV2();
            result5.Percentage = 90;
            Console.WriteLine(result5.ComputeHonors());
            Console.ReadKey();
        }
    }
}
