﻿using System;
using Wiskunde.Meetkunde;
using OOP;

namespace LerenWerkenMetOOP
{
    class Program
    {
        public static void Main(string[] args)
        {
            KennismakingMetOO nieuw = new KennismakingMetOO();

            bool done = false;
            while (!done)
            {
                Console.WriteLine("Oefeningen Cauwenbergh Sven");
                Console.WriteLine("1: Beginnen met OO");
                Console.WriteLine("2: Kennismaking met OO");
                Console.WriteLine("3: DateTime: leren werken met objecten");
                Console.WriteLine("4: MethodsModifiersProperty");
                Console.WriteLine("5: H9 Geheugenmanagement bij klassen");
                Console.WriteLine("6: Spelen met strings");
                Console.WriteLine("Q. Het is genoeg voor vandaag!");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (response)
                {
                    case '1':
                        SubMenuBeginnenMetOO();
                        break;
                    case '2':
                        nieuw.SubMenuKennismakingMetOO(); // zonder static methode in klasse KennismakingMetOO
                        break;
                    case '3':
                        LerenWerkenMetDateTime.SubMenuDateTime();
                        break;
                    case '4':
                        MethodsModifiersProperties.SubMenuMethodsModifiersProperties();
                        break;
                    case '5':
                        OOP.GeheugenmanagementBijKlassen.SubMenuGeheugenmanagement();
                        break;
                    case '6':
                       OOP.GeavanceerdeKlassenEnObjecten.SpelenMetSTrings();
                        break;
                    case 'Q':
                    case 'q':
                        Console.WriteLine("Wil je ermee stoppen? J/N");
                        char exitResponse = Console.ReadKey().KeyChar;
                        if (exitResponse == 'J' || exitResponse == 'j')
                        {
                            done = true;
                        }
                        break;
                }
                Console.Clear();
            }
            Console.ReadKey();
        }
        static void SubMenuBeginnenMetOO()
        {
            bool done = false;
            Console.Clear();
            while (!done)
            {
                Console.WriteLine("Submenu Beginnen met OO");
                Console.WriteLine("1. Wat is static?");
                Console.WriteLine("2. Public vs Private");
                Console.WriteLine("3. Teken een lijn");
                Console.WriteLine("4. Teken een lijn met een *");
                Console.WriteLine("5. Teken een lijn met 20 *en");
                Console.WriteLine("6. Teken een lijn met de standaard static teken waarde");
                Console.WriteLine("7. Teken een lijn met de static teken waarde = +");
                Console.WriteLine("8. Teken een rechthoek 10 tekens lang en 3 lijnen hoog");
                Console.WriteLine("9. Teken een volle rechthoek en geef hoogte en breedte op");
                Console.WriteLine("a. Teken een lege rechthoek en geef hoogte en breedte op");
                Console.WriteLine("Q. Terug naar het hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        WatIsStatic();
                        break;
                    case '2':
                        PrivateVsPublic();
                        break;
                    case '3':
                        /* ik kan de methode Lijn oproepen
                         * omdat die static is en ik hoef
                         * dus niet eerst een object of instantie
                         * te maken */
                        Console.WriteLine(Vormen.Lijn());
                        break;
                    case '4':
                        Console.WriteLine(Vormen.Lijn('*'));
                        break;
                    case '5':
                        Console.WriteLine(Vormen.Lijn('*', 20));
                        break;
                    case '6':
                        Console.WriteLine(Vormen.LijnDieHetVeldMetDeNaamTekenGebruikt());
                        break;
                    case '7':
                        Vormen.teken = '+';
                        Console.WriteLine(Vormen.LijnDieHetVeldMetDeNaamTekenGebruikt());
                        break;
                    case '8':
                        Console.WriteLine(Vormen.VolleRechthoek());
                        break;
                    case '9':
                        Console.WriteLine(Vormen.VolleRechthoek(20, 20));
                        break;
                    case 'a':
                        Console.WriteLine(Vormen.LegeRechthoek(20, 20));
                        break;

                    case 'Q':
                        done = true;
                        break;
                }
            }
        }

        static void WatIsStatic()
        {
            Console.WriteLine("Leren werken met DGP 'ding gericht programmeren!");
            // instantie of exemplaar van de klasse maken
            // Wiskunde.Meetkunde.Vormen vormen = new Wiskunde.Meetkunde.Vormen();
            // de methode van de instantie of het object gebruiken
            // Console.WriteLine(vormen.Lijn());
            // als de methode lijn static is, behoort die tot
            // de klasse en niet tot het object
            Console.WriteLine(Wiskunde.Meetkunde.Vormen.Lijn());
        }

        static void PrivateVsPublic()
        {
            // het veld kleur van de klasse Vormen is hier
            // zichtbaar omdat het public is
            Wiskunde.Meetkunde.Vormen.kleur = ConsoleColor.Red;
            Wiskunde.Meetkunde.Vormen vormen = new Wiskunde.Meetkunde.Vormen();
            // het object, instantie of exemplaar geeft geen toegang 
            // tot static methode
            // vormen.LijnInKleuren();
            Wiskunde.Meetkunde.Vormen.Achtergrond = ConsoleColor.Cyan;
            Wiskunde.Meetkunde.Vormen.LijnInKleur();
            // welke kleur wordt door Vormen gebruikt?
            if (Wiskunde.Meetkunde.Vormen.Achtergrond == ConsoleColor.Red)
            {
                Console.WriteLine("De achtergrondkleur is rood.");
            }
            else
            {
                Console.WriteLine("De achtergrondkleur is niet rood.");

            }

            
        }

    }
}
