﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeheugenmanagementBijKlassen
    {


    public static void SubMenuGeheugenmanagement()
        {
            bool done = false;
            Console.Clear();
            while (!done)
            {
                Console.WriteLine("Oefeningen Cauwenbergh Sven");
                Console.WriteLine("1: H9 PokemonAttack oefening 1");
                Console.WriteLine("2: H9 PokemonAttack oefening 2");
                Console.WriteLine("3: H9 PokemonAttack oefening 3");
                Console.WriteLine("4: H10 PokemonAttack oefening 1");
                Console.WriteLine("5: H10 PokemonAttack oefening 2");
                Console.WriteLine("Q. Het is genoeg voor vandaag!");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        Pokemon.MakePokemon();
                        break;
                    case '2':
                        Pokemon.TestConsciousPokemon();
                        break;
                    case '3':
                        Pokemon.testConciousPokemonSafe();
                        break;
                    case '4':
                        Pokemon.MakePokemonWithConstructor();
                        break;
                    case '5':
                        Pokemon.MakePokemonChained();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }

            }
        }
    }
}
