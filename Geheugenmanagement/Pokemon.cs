﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
	public enum PokeSpecies { Bulbasaur, Charmander, Squirtle, Pikachu };
	public enum PokeTypes { Grass, Fire, Water, Electric };

	class Pokemon
	{
		private int maxHP;

		public int MaxHP
		{
			get { return maxHP; }
			set
			{
				if (value <= 1000)
				{
					if (value >= 20)
					{
						maxHP = value;
					}
					else
					{
						maxHP = 20;
					}
				}
				else
				{
					maxHP = 1000;
				}
			}
		}

		private int hp;

		public int HP
		{
			get { return hp; }
			set
			{
				if (value <= 0)
				{
					hp = 0;
				}
				else if (value >= MaxHP)
				{
					hp = MaxHP;
				}
				else
				{
					hp = value;
				}
			}
		}


		public PokeSpecies PokeSpecie { get; set; }
		public PokeTypes PokeType { get; set; }

		public Pokemon() { }
		public Pokemon(int health, int maxHealth, PokeSpecies pokeSpecie, PokeTypes pokeType)
		{
			MaxHP = maxHealth;
			HP = health;
			PokeSpecie = pokeSpecie;
			PokeType = pokeType;
		}
		public Pokemon(int maxHealth, PokeSpecies pokeSpecie, PokeTypes pokeType) : this(maxHealth, maxHealth, pokeSpecie, pokeType)

		{
			HP = maxHealth / 2;
		}


		public void Attack()
		{
			if (PokeType == PokeTypes.Grass)
			{
				Console.ForegroundColor = ConsoleColor.Green;

			}
			else if (PokeType == PokeTypes.Water)
			{
				Console.ForegroundColor = ConsoleColor.Blue;
			}

			else if (PokeType == PokeTypes.Fire)
			{
				Console.ForegroundColor = ConsoleColor.Red;
			}
			else if (PokeType == PokeTypes.Electric)
			{
				Console.ForegroundColor = ConsoleColor.DarkYellow;
			}
			Console.WriteLine($"{PokeSpecie.ToString().ToUpper()}! {HP}");
			Console.ResetColor();


		}
		public static void MakePokemon()
		{
			Pokemon bulbasaur = new Pokemon
			{
				PokeSpecie = PokeSpecies.Bulbasaur,
				PokeType = PokeTypes.Grass,
				MaxHP = 20,
				HP = 20
			};
			bulbasaur.Attack();

			Pokemon charmander = new Pokemon();
			charmander.PokeSpecie = PokeSpecies.Charmander;
			charmander.PokeType = PokeTypes.Fire;
			charmander.MaxHP = 20;
			charmander.HP = 20;
			charmander.Attack();

			Pokemon squirtle = new Pokemon();
			squirtle.PokeSpecie = PokeSpecies.Squirtle;
			squirtle.PokeType = PokeTypes.Water;
			squirtle.MaxHP = 20;
			squirtle.HP = 20;
			squirtle.Attack();

			Pokemon pikachu = new Pokemon();
			pikachu.PokeSpecie = PokeSpecies.Pikachu;
			pikachu.PokeType = PokeTypes.Electric;
			pikachu.MaxHP = 20;
			pikachu.HP = 20;
			pikachu.Attack();
			Console.ReadKey();
		}

		public static void MakePokemonWithConstructor()
		{
			Pokemon bulbasaur = new Pokemon(5, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
			Console.WriteLine($"{bulbasaur.PokeSpecie} {bulbasaur.PokeType}pokemon heeft {bulbasaur.HP} van {bulbasaur.MaxHP} over ");
			bulbasaur.Attack();

			Pokemon squirtle = new Pokemon(10, 20, PokeSpecies.Squirtle, PokeTypes.Water); //andere constructor! 4 variablen ipv 3!
			Console.WriteLine($"{squirtle.PokeSpecie} {squirtle.PokeType}pokemon heeft {squirtle.HP} van {squirtle.MaxHP} over");
			squirtle.Attack();

			Pokemon charmander = new Pokemon(15, 20, PokeSpecies.Charmander, PokeTypes.Fire);
			Console.WriteLine($"{charmander.PokeSpecie} {charmander.PokeType}pokemon heeft {charmander.HP} van {charmander.MaxHP} over");
			charmander.Attack();

			Pokemon pikachu = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);
			Console.WriteLine($"{pikachu.PokeSpecie} {pikachu.PokeType}pokemon heeft {pikachu.HP} van {pikachu.MaxHP} over");
			pikachu.Attack();

			Console.WriteLine("pokemons aangemaakt");
		}

		public static void MakePokemonChained()
		{
			Pokemon bulbasaur = new Pokemon(10, PokeSpecies.Bulbasaur, PokeTypes.Grass);
			Console.WriteLine($"{bulbasaur.PokeSpecie} {bulbasaur.PokeType}pokemon heeft {bulbasaur.HP} van {bulbasaur.MaxHP} over ");
			bulbasaur.Attack();

			Pokemon squirtle = new Pokemon(20, PokeSpecies.Squirtle, PokeTypes.Water);
			Console.WriteLine($"{squirtle.PokeSpecie} {squirtle.PokeType}pokemon heeft {squirtle.HP} van {squirtle.MaxHP} over");
			squirtle.Attack();

			Pokemon charmander = new Pokemon(20, 50, PokeSpecies.Charmander, PokeTypes.Fire);
			Console.WriteLine($"{charmander.PokeSpecie} {charmander.PokeType}pokemon heeft {charmander.HP} van {charmander.MaxHP} over");
			charmander.Attack();

			Pokemon pikachu = new Pokemon(100, PokeSpecies.Pikachu, PokeTypes.Electric);
			Console.WriteLine($"{pikachu.PokeSpecie} {pikachu.PokeType}pokemon heeft {pikachu.HP} van {pikachu.MaxHP} over");
			pikachu.Attack();

			Console.WriteLine("pokemons aangemaakt");
		}

		public static Pokemon FirstConsciousPokemon(Pokemon[] pokemons)
		{
			for (int i = 0; i < pokemons.Length; i++)
			{
				if (pokemons[i].HP >= 1)
				{
					return pokemons[i];
				}
			}
			return null;
		}
		public static void TestConsciousPokemon()
		{
			// De 4 pokemon maken en in array stoppen
			//  Bulbasaur en Charmander 0 HP hebben
			// Squirtle 2 HP en Picachu 20 HP
			Pokemon bulbasaur = new Pokemon();
			bulbasaur.MaxHP = 20;
			bulbasaur.HP = 0;
			bulbasaur.PokeSpecie = PokeSpecies.Bulbasaur;
			bulbasaur.PokeType = PokeTypes.Grass;

			Pokemon charmander = new Pokemon();
			charmander.MaxHP = 20;
			charmander.HP = 3;
			charmander.PokeSpecie = PokeSpecies.Charmander;
			charmander.PokeType = PokeTypes.Fire;

			Pokemon squirtle = new Pokemon();
			squirtle.MaxHP = 20;
			squirtle.HP = 2;
			squirtle.PokeSpecie = PokeSpecies.Squirtle;
			squirtle.PokeType = PokeTypes.Water;

			Pokemon pikachu = new Pokemon();
			pikachu.MaxHP = 20;
			pikachu.HP = 20;
			pikachu.PokeSpecie = PokeSpecies.Pikachu;
			pikachu.PokeType = PokeTypes.Electric;

			// we stoppen de instanties die we gemaakt hebben in een array
			Pokemon[] pokemonArray = { bulbasaur, charmander, squirtle, pikachu };
			// vermits FirstConsciousPokemon een static methode is
			// kunnen we die direct vanuit de klasse oproepen en niet
			// vanuit een object van die klasse
			Pokemon firstPokemon = Pokemon.FirstConsciousPokemon(pokemonArray);
			firstPokemon.Attack();
			Console.ReadKey();
		}
		public static void testConciousPokemonSafe()
		{
			Pokemon bulbasaur = new Pokemon();
			bulbasaur.MaxHP = 20;
			bulbasaur.HP = 5;
			bulbasaur.PokeSpecie = PokeSpecies.Bulbasaur;
			bulbasaur.PokeType = PokeTypes.Grass;

			Pokemon charmander = new Pokemon();
			charmander.MaxHP = 20;
			charmander.HP = 5;
			charmander.PokeSpecie = PokeSpecies.Charmander;
			charmander.PokeType = PokeTypes.Fire;

			Pokemon squirtle = new Pokemon();
			squirtle.MaxHP = 20;
			squirtle.HP = 5;
			squirtle.PokeSpecie = PokeSpecies.Squirtle;
			squirtle.PokeType = PokeTypes.Water;

			Pokemon pikachu = new Pokemon();
			pikachu.MaxHP = 0;
			pikachu.HP = 5;
			pikachu.PokeSpecie = PokeSpecies.Pikachu;
			pikachu.PokeType = PokeTypes.Electric;

			Pokemon[] pokemonArray = { bulbasaur, charmander, squirtle, pikachu };

			Pokemon firstPokemon = Pokemon.FirstConsciousPokemon(pokemonArray);

			if (firstPokemon != null)
			{
				firstPokemon.Attack();
			}
			else
			{
				Console.WriteLine("Alle Pokemon dood");
			}
			Console.ReadKey();
		}
	}
}
