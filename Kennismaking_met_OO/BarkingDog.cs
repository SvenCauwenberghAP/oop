﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class BarkingDog
    {
        // nu maken we onze randomgenerator *buiten* Main
        public string Name;
        public string Breed;

        /*  public static void Main()
          {
              BarkingDog nieuw = new BarkingDog();
              nieuw.BarkingDogs();
          }*/
        public static void BarkingDogs()
        {
            // bepaal hier met de randomgenerator het ras van de hond
            Random rng = new Random();
            BarkingDog dog1 = new BarkingDog();
            BarkingDog dog2 = new BarkingDog();
            // stel hier `Breed` in
            // maak van `Breed` een eigenschap zoals we dat bij de geometrische vormen hebben gedaan
            int dog1BreedNumber = rng.Next(0, 3);
            int dog2BreedNumber = rng.Next(0, 3);
            dog1.Name = "Swieber";
            dog2.Name = "Misty";
            if (dog1BreedNumber == 0)
            {
                dog1.Breed = "German Shepherd";
            }
            else if (dog1BreedNumber == 1)
            {
                dog1.Breed = "Wolfspitz";
            }
            else
            {
                dog1.Breed = "Chihuahua";
            }
            if (dog2BreedNumber == 0)
            {
                dog2.Breed = "German Shepherd";
            }
            else if (dog2BreedNumber == 1)
            {
                dog2.Breed = "Wolfspitz";
            }
            else
            {
                dog2.Breed = "Chihuahua";
            }

            Console.WriteLine(dog1.Bark());
            Console.WriteLine(dog2.Bark());

            Console.ReadKey();
            Console.Clear();
        }
        public string Bark()
        {
            if (Breed == "German Shepherd")
            {
                return "RUFF!";
            }
            else if (Breed == "Wolfspitz")
            {
                return "AwawaWAF!";
            }
            else if (Breed == "Chihuahua")
            {
                return "ARF ARF ARF!";
            }
            // dit zou nooit mogen gebeuren
            // maar als de programmeur van Main iets fout doet, kan het wel
            else
            {
                return "Euhhh... Miauw?";
            }
        }

    }
}