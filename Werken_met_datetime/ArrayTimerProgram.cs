﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArrayTimerProgram
    {
        public static void Main()
        {
            int aantal = 1000000;
            DateTime beginTime = DateTime.Now;
            int[] miljoen = new int[aantal];
            for (int i = 1; i <= aantal; i++)
            {
                miljoen[i - 1] = i;
            }
            DateTime endTime = DateTime.Now;
            TimeSpan difference = endTime - beginTime;
            Console.WriteLine($"Het duurt {difference.TotalMilliseconds} milliseconden om een array van {aantal} elementen aan " +
                              $"te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}
