﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GetallenCombinatie
    {
        public int Number1 { get; set; }

        public int Number2 { get; set; }

        public double Sum()
        {
            return Number1 + Number2;
        }
        public double Difference()
        {
            return Number1 - Number2;
        }
        public double Product()
        {
            return Number1 * Number2;
        }
        public double Quotient()
        {
            if(Number2 != 0)
            {
                return (Number1 / Number2);
            }
            else
            {
                Console.WriteLine("ERROR");
                return 0;
            }
            
        }

        public static void Getallen()
        {
            GetallenCombinatie pair1 = new GetallenCombinatie();
            pair1.Number1 = 12;
            pair1.Number2 = 34;
            Console.WriteLine("Paar:" + pair1.Number1 + ", " + pair1.Number2);
            Console.WriteLine("Sum = " + pair1.Sum());
            Console.WriteLine("Verschil = " + pair1.Difference());
            Console.WriteLine("Product = " + pair1.Product());
            Console.WriteLine("Quotient = " + pair1.Quotient());
        }
    }
}
