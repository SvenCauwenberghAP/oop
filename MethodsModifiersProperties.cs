﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class MethodsModifiersProperties
    {
        public static void SubMenuMethodsModifiersProperties()
        {
            bool done = false;
            Console.Clear();
            while (!done)
            {
                Console.WriteLine("Oefeningen Cauwenbergh Sven");
                Console.WriteLine("1: De Studentklasse testen");
                Console.WriteLine("2: Rapport Module V1");
                Console.WriteLine("3: Rapport Module V2");
                Console.WriteLine("4: H8 Figuren");
                Console.WriteLine("5: H8 Getallencombinatie");
                Console.WriteLine("Q. Terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        OOP.Student.TestStudent();
                        break;
                    case '2':
                        OOP.ResultV1.StartRapportModule();
                        break;
                    case '3':
                        OOP.ResultV2.StartRapportModule2();
                        break;
                    case '4':
                      //  OOP.Figuren.vierhoeken();
                        break;
                    case '5':
                        OOP.GetallenCombinatie.Getallen();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }

    }
}
