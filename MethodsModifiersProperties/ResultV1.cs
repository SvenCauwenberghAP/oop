﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV1
    {
        public byte Percentage { get; set; }

        public static void StartRapportModule()
        {
            OOP.ResultV1 graad1 = new ResultV1();
            graad1.Percentage = 40;
            graad1.PrintHonors();
            OOP.ResultV1 graad2 = new ResultV1();
            graad2.Percentage = 60;
            graad2.PrintHonors();
            OOP.ResultV1 graad3 = new ResultV1();
            graad3.Percentage = 80;
            graad3.PrintHonors();
            OOP.ResultV1 graad4 = new ResultV1();
            graad4.Percentage = 90;
            graad4.PrintHonors();
            OOP.ResultV1 graad5 = new ResultV1();
            graad5.Percentage = 0;
            graad5.PrintHonors();
            Console.ReadKey();
        }
        public void PrintHonors()
        {
            if (Percentage < 50)
            {
                Console.WriteLine("Gebuisd!");
            }
            else
            {
                if (Percentage <= 68)
                {
                    Console.WriteLine("Niptekes!");
                }
                else
                {
                    if (Percentage <= 75)
                    {
                        Console.WriteLine("Onderscheiding!");
                    }
                    else
                    {
                        if (Percentage <= 85)
                        {
                            Console.WriteLine("Grote onderscheiding");
                        }
                        else
                        {
                            Console.WriteLine("Grootste onderscheiding!");
                        }
                    }

                }
            }
        }
    }
}
