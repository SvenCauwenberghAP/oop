﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Ticks2000Program
    {
        public static void Main()
        {
            int start = 2020;
            DateTime currentTime = DateTime.Now;
            DateTime millenium = new DateTime(start, 1, 1);
            long elapsedTick = currentTime.Ticks - millenium.Ticks;
            Console.WriteLine($"Sinds 1 januari {start} zijn er {elapsedTick} ticks voorbijgegaan.");
        }
    }
}
