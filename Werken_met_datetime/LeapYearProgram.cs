﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class LeapYearProgram
    {
        public static void Main()
        {
            int numberOfLeapYears = 0;
            for (int i = 1800; i <= 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    numberOfLeapYears++;
                }
            }
            Console.WriteLine($"Er zijn {numberOfLeapYears} schrikkeljaren tussen 1800 en 2020");
        }
    }
}
