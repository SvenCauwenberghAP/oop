﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class KennismakingMetOO
    {
        public void SubMenuKennismakingMetOO()
        {
            bool done = false;
            Console.Clear();
            while (!done)
            {
                Console.WriteLine("Oefeningen Cauwenbergh Sven");
                Console.WriteLine("1: H8 Auto");
                Console.WriteLine("2: H8 Blaffende honden");
                Console.WriteLine("Q. Het is genoeg voor vandaag!");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                      Auto.AutoLatenRijden();
                        break;
                    case '2':
                       BarkingDog.BarkingDogs();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }
    }
}
